document.addEventListener("DOMContentLoaded", function() {
    var elements = document.querySelectorAll("#first_name, #last_name");
    var emailInput = document.getElementById("email")
    for (var i = 0; i < elements.length; i++) {
        elements[i].oninvalid = function(e) {
            e.target.setCustomValidity("");
            if (!e.target.validity.valid) {
                e.target.setCustomValidity("Required Field");
            }
        };
        elements[i].oninput = function(e) {
            e.target.setCustomValidity("");
        };
    }
    emailInput.oninvalid = function(e) {
        e.target.setCustomValidity("");
        if (!e.target.validity.valid) {
            e.target.setCustomValidity("Invalid Email Address. Try again.");
        }
    };
})